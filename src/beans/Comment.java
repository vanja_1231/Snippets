/**
 * 
 */
package beans;

import java.util.Date;

/**
 * @author Vanja
 *
 */
public class Comment {
	private String text;
	private java.util.Date date;
	private User user;
	private int rating;
	public Comment() {
		super();
	}
	public Comment(String text, Date date, User user, int rating) {
		super();
		this.text = text;
		this.date = date;
		this.user = user;
		this.rating = rating;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public java.util.Date getDate() {
		return date;
	}
	public void setDate(java.util.Date date) {
		this.date = date;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	@Override
	public String toString() {
		return "Comment [text=" + text + ", date=" + date + ", user=" + user
				+ ", rating=" + rating + "]";
	}
	
}
