/**
 * 
 */
package beans;

/**
 * @author Vanja
 *
 */
public class Rating {
	private int likes;
	private int dislikes;
	
	public Rating() {
		super();
	}
	public Rating(int likes, int dislikes) {
		super();
		this.likes = likes;
		this.dislikes = dislikes;
	}
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	public int getDislikes() {
		return dislikes;
	}
	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}
	@Override
	public String toString() {
		return "Rating [likes=" + likes + ", dislikes=" + dislikes + "]";
	}
	
}
