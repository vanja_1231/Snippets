/**
 * 
 */
package beans;

import java.net.URL;

/**
 * @author Vanja
 *
 */
public class Snippet {
	private String description;
	private String code;
	private ProgrammingLanguage progLanguage;
	private URL url;
	private int duration;
	
	
	
	public Snippet() {
		super();
	}



	public Snippet(String description, String code,
			ProgrammingLanguage progLanguage, URL url, int duration) {
		super();
		this.description = description;
		this.code = code;
		this.progLanguage = progLanguage;
		this.url = url;
		this.duration = duration;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public ProgrammingLanguage getProgLanguage() {
		return progLanguage;
	}



	public void setProgLanguage(ProgrammingLanguage progLanguage) {
		this.progLanguage = progLanguage;
	}



	public URL getUrl() {
		return url;
	}



	public void setUrl(URL url) {
		this.url = url;
	}



	public int getDuration() {
		return duration;
	}



	public void setDuration(int duration) {
		this.duration = duration;
	}



	@Override
	public String toString() {
		return "Snippet [description=" + description + ", code=" + code
				+ ", progLanguage=" + progLanguage + ", url=" + url
				+ ", duration=" + duration + "]";
	}
	
	
}
