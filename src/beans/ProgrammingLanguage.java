/**
 * 
 */
package beans;

/**
 * @author Vanja
 *
 */
public class ProgrammingLanguage {
	private String language;


	public ProgrammingLanguage() {
		super();
	}

	public ProgrammingLanguage(String language) {
		super();
		this.language = language;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
}
