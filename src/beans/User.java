/**
 * 
 */
package beans;

import java.io.Serializable;

/**
 * @author Vanja
 *
 */
public class User implements Serializable {
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private Role role;
	private String phone;
	private	String email;
	private String address;
	private String picture;
	
	public User() {
		super();
	}

	public User(String userName, String password, String firstName,
			String lastName, Role role, String phone, String email,
			String address, String picture) {
		super();
		this.username = userName;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.role = role;
		this.phone = phone;
		this.email = email;
		this.address = address;
		this.picture = picture;
	}

	public String getUserName() {
		return username;
	}

	public void setUserName(String userName) {
		this.username = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	@Override
	public String toString() {
		return "User [userName=" + username + ", password=" + password
				+ ", firstName=" + firstName + ", lastName=" + lastName
				+ ", role=" + role + ", phone=" + phone + ", email=" + email
				+ ", address=" + address + ", picture=" + picture + "]";
	}
	
}

