/**
 * 
 */
package beans;

/**
 * @author Vanja
 *
 */
public enum Role {
	ADMINISTRATOR,
	REGISTERED_USER,
	GUEST;
	
	private String role;
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	
}
