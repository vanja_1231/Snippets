package controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.annotation.JSONP;

import services.DataHandler;
import beans.Role;
import beans.User;

/**
 * @author Vanja
 *
 */

@Path("/user")
public class UserService {

	@POST
	@Path("/register")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public void register(@FormDataParam("firstName") String firstName,
			@FormDataParam("lastName") String lastName,
			@FormDataParam("username") String username,
			@FormDataParam("password") String password,
			@FormDataParam("phone") String phone,
			@FormDataParam("email") String email,
			@FormDataParam("address") String address,
			@FormDataParam("picture") InputStream picture,
			@FormDataParam("picture") FormDataContentDisposition fileDetails)
			throws JsonProcessingException, URISyntaxException {

		User registered = new User(username, password, firstName, lastName,
				Role.REGISTERED_USER, phone, email, address, fileDetails.getFileName());
		String uploadedFileLocation = "d://uploaded/"+ fileDetails.getFileName();

		// save uploaded picture to file
		DataHandler.savePicture(picture, uploadedFileLocation);

		String fileName = "/services/registered.ser";
		ArrayList<User> read = new ArrayList<User>();
		read = DataHandler.deserialize(fileName);
		read.add(registered);
		
		//System.out.println(read.size());
		DataHandler.serialize(read, fileName);

		java.net.URI location = new java.net.URI("../../Snippet/success.html");
		// return Response.temporaryRedirect(location).build();
	}

	@POST
	@Path("/logina")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String testform1(@FormParam("userName") String userName,
			@FormParam("password") String password) {
		System.out.println("dosaao");
		return "/rest/demo/login received @FormParam('userName') " + userName
				+ ", and @FormParam('password'): " + password;
	}

	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public User login(@Context HttpServletRequest request, User user) {
		User retVal = null;
		retVal = (User) request.getSession().getAttribute("user");
		if (retVal == null) {
			request.getSession().setAttribute("user", user);
			retVal = user;
		}
		return retVal;
	}

	@GET
	@Path("/testlogin")
	@Produces(MediaType.TEXT_PLAIN)
	public String testLogin(@Context HttpServletRequest request) {
		User retVal = null;
		retVal = (User) request.getSession().getAttribute("user");
		if (retVal == null) {
			return "No user logged in.";
		}
		return "User " + retVal + " logged in.";
	}

	@GET
	@Path("/users")
	@Produces(MediaType.APPLICATION_JSON)
	public String loadUsers(@Context HttpServletRequest request)
			throws JsonProcessingException {
		ArrayList<User> read = new ArrayList<User>();
		read = DataHandler.deserialize("/services/registered.ser");
		ObjectMapper mapper = new ObjectMapper();
		// Object to JSON in String
		String jsonInString = mapper.writeValueAsString(read);
		return jsonInString;
	}

}
