/**
 * 
 */
package services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang.SerializationUtils;

import java.net.URISyntaxException;
import java.net.URL;

import beans.User;

/**
 * @author Vanja
 *
 */
public class DataHandler {

	public static void serialize(Serializable object, String fileName) {
		try {
			URL resourceUrl = DataHandler.class.getClassLoader().getResource(fileName);
			File file = new File(resourceUrl.toURI());
			FileOutputStream fos = new FileOutputStream(file);
			SerializationUtils.serialize(object, fos);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> ArrayList<T> deserialize(String fileName) {
		 ArrayList<T> retVal = null;
		try {
			 InputStream is = DataHandler.class.getClassLoader().getResourceAsStream(fileName);
			 retVal = (ArrayList<T>) SerializationUtils.deserialize(is);
			is.close();
			return retVal;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	// save uploaded file to new location
		public static void savePicture(InputStream uploadedInputStream,
				String uploadedFileLocation) {

			try {
				OutputStream out = new FileOutputStream(new File(
						uploadedFileLocation));
				int read = 0;
				byte[] bytes = new byte[1024];

				out = new FileOutputStream(new File(uploadedFileLocation));
				while ((read = uploadedInputStream.read(bytes)) != -1) {
					out.write(bytes, 0, read);
				}
				out.flush();
				out.close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
}